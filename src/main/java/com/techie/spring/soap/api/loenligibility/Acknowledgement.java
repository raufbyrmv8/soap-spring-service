package com.techie.spring.soap.api.loenligibility;

import jakarta.xml.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "isEligible",
        "approvedAmount",
        "criteriaMismatch"
})
@XmlRootElement(name = "Acknowledgement")
public class Acknowledgement {
    protected boolean isEligible;
    protected long approvedAmount;
    @XmlElement(name = "CriteriaMismatch", required = true)
    protected List<String> criteriaMismatch;

    public boolean isIsEligible() {
        return isEligible;
    }

    public void setIsEligible(boolean value) {
        this.isEligible = value;
    }

    public long getApprovedAmount() {
        return approvedAmount;
    }

    public void setApprovedAmount(long value) {
        this.approvedAmount = value;
    }

    public List<String> getCriteriaMismatch() {
        if (criteriaMismatch == null) {
            criteriaMismatch = new ArrayList<String>();
        }
        return this.criteriaMismatch;
    }
}
