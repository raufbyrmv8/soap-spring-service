package com.techie.spring.soap.api.loenligibility;


import jakarta.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {
    public ObjectFactory() {
    }
    public CustomerRequest createCustomerRequest() {
        return new CustomerRequest();
    }
    public Acknowledgement createAcknowledgement() {
        return new Acknowledgement();
    }
}
